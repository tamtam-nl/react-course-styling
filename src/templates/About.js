import React from 'react'

export default class About extends React.Component {
  render() {
    return (
        <div>
            <h1>About</h1>
            <p>This is an about page</p>
            <button>
                Contact us
            </button>
        </div>
    )
  }
}

