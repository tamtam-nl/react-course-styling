import React from 'react'

export default class Contact extends React.Component {
  render() {
    return (
        <form>
          <input placeholder="Your name"/>
          <input placeholder="Your email"/>
          <textarea placeholder="Your message"></textarea>
          <button>Submit</button>
        </form>
    )
  }
}
