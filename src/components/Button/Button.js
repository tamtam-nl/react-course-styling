import React from 'react'

export default class CoolButton extends React.Component {
    render(){
        return (
            <button>{this.props.text}</button>
        )
    }
}
