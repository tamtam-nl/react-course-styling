import React from 'react'
import {Link} from 'react-router'

export default class MainNav extends React.Component {
    render() {
        return (
            <ul>
              <li><Link to={'/'}>Home</Link></li>
              <li><Link to={'/about'}>About</Link></li>
              <li><Link to={'/people'}>People</Link></li>
              <li><Link to={'/contact'}>Contact</Link></li>
            </ul>
        )
    }
}