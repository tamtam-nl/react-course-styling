module.exports = {
    routes: [
        '/',
        '/about',
        '/people',
        '/contact'
    ]
}